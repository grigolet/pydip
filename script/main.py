from subprocess import Popen, PIPE, CalledProcessError
from collections import namedtuple
from datetime import datetime
import json
from influxdb import InfluxDBClient

db_configs = json.load(open('/config/db.json'))
params = json.load(open('/config/params.json'))
Message = namedtuple('Message', 'time subscription tag value_type_num value')
cmd = ['./dip']
client = InfluxDBClient(db_configs['url'], db_configs['port'], db_configs['user'],
                        db_configs['password'], db_configs['db'], db_configs['ssl'],
                        db_configs['verify_ssl'])


value_map = {
    '1': int,
    '7': float,
    '6': float,
    '4': int,
    '5': int,
    '0': None,
    '8': str
}

types_map = {
    bool: 'Bool',
    float: 'Float',
    int: 'Int',
    str: 'String'
}


def parse_line(line):
    values = line.rstrip().split(' ')
    message = Message(int(values[0]), values[1], values[2], values[3], values[4])
    return message


def write_point(client, message, value_map):
    field_type = value_map[message.value_type_num]
    string_type = types_map[field_type]
    point = [
        {
            "measurement": "dip",
            "tags": {
                "sub": message.subscription,
                "dip_tag": message.tag
            },
            "time": datetime.utcfromtimestamp(message.time // 1000000000).strftime("%Y-%m-%d %H:%M:%S.%f"),
            "fields": {
                f"{string_type}_value": field_type(message.value)
            }
        }
    ]
    client.write_points(point, time_precision='u')
    print(message, flush=True)


with Popen(cmd, stdout=PIPE, bufsize=1, universal_newlines=True) as p:
    data = {}
    for line in p.stdout:
        # parse the data from the line of stdout
        if not line.split(' ')[0].isdigit(): continue
        message = parse_line(line)

        data[message.subscription] = data.get(message.subscription, {})
        data[message.subscription][message.tag] = data[message.subscription].get(message.tag, {})
        point = data[message.subscription][message.tag] 
        point['timestamp'] = point.get('timestamp', 0)
        point['value'] = point.get(value_map[message.value_type_num](message.value))


        if message.time - point['timestamp'] > params["thresh_time"] * 1e9:  # in nanoseconds
            write_point(client, message, value_map)
            data[message.subscription][message.tag]['timestamp'] = message.time

if p.returncode != 0:
    raise CalledProcessError(p.returncode, p.args)
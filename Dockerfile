FROM ubuntu:latest

RUN apt-get update && \
    apt-get install -y gcc g++ wget unzip make python3 python3-pip -y && \
    wget https://nexus.web.cern.ch/nexus/service/local/repositories/cern-nexus/content/cern/dip/dip/5.6.3/dip-5.6.3-distribution.zip -O /tmp/dip.zip && \
    unzip /tmp/dip.zip && \
    mv dip-5.6.3 dip

# Need to set environment variable for linking log4cplus
ENV DIPBASE=/dip
ENV LD_LIBRARY_PATH=$DIPBASE/lib64:$LD_LIBRARY_PATH
WORKDIR /app
COPY Makefile dip.cpp ./
RUN make
COPY config/ /config/
COPY script/ /script/
RUN pip3 install influxdb
USER pydip
ENTRYPOINT ["python3", "/script/main.py"]

# pydip

Wrapper around CERN DIP api written in C++ and python. The code is run on a docker 
image where the dependencies are built and a c++ program is compiled. The program reads
from a .txt file the subscriptions and prints them into the stdout. A python script launch
the compiled program and reads from the stdin, parsing the arguments.

The list of subcriptions is under `config/list.txt`. Each subscription should be on a new line

The config parameters are in the `config/params.json`. 
**Description**:
* `thresh_time`: maximum interval time of sampling data. Default: 2s

You have to configure a `/config/db.json` mounted as a volume or config map
```json
{
    "url": "databaseurl.cern.ch",
    "port": 8086,
    "user": "username",
    "password": "password",
    "db": "name_of_database",
    "ssl": true,
    "verify_ssl": false
}
```

and a `/config/params.json` (optional) to override the defaul parameters:
```json
{
    "thresh_time": 2
}
```

To build the image:
```bash
docker build . -t pydip:latest
```

To run the docker image:
```bash
docker run -e LIST_PATH=/config/list.txt pydip:latest 
``` 
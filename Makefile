#
# Makefile for c++ dip client
# Author: gianluca rigoletti <gianluca.rigoletti@cern.ch>
#

CC = g++
CFLAGS = -Wall -ansi -I /dip/include -L /dip/lib64 -std=c++11
BOOST = -I /home/gianluca/.conda/envs/dip/include/boost
DLIB = /dip/lib64/libdip.so  -lpthread -ldl
LINK = -Wl,-rpath=/dip/lib64

all:	Client

Client:	dip.cpp /dip/lib64/libdip.so
	$(CC) $(CFLAGS) $(BOOST) dip.cpp $(DLIB) /dip/lib64/liblog4cplus.so  -o dip $(LIBS)


#include <unistd.h>
#include "Dip.h"
#include "DipSubscription.h"
#include "DIPVersion.h"
#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <chrono>
using namespace std;

//How long the test will run in ms. Default is 1 minute.
unsigned int lifeTime;

/**
* Simple demo client that subscribes to data coming from servers.
* Subscribe to "dip/test/api/dip-server_<from 0 to noPubs>"
*/

class Client
{

private:
	// hold reference to subscription objects
	DipSubscription **sub;

	// DIP object
	DipFactory *dip;

	/**
* handler for connect/disconnect/data reception events
* Nested class
* */
	class GeneralDataListener : public DipSubscriptionListener
	{
	private:
		// allow us to access subscription objects
		Client *client;

		//	bool flip;

	public:
		GeneralDataListener(Client *c) : client(c){};

		/**
	* handle changes to subscribed to publications
	* Simply prints the contents of the received data.
	* @param subscription - the subsciption to the publications thats changed.
	* @param message - object containing publication data
	* */
		void handleMessage(DipSubscription *subscription, DipData &message)
		{
			int ntags;
			const char** tags = message.getTags(ntags);
			for (int i = 0; i < ntags; i++)
			{
				const char* tag = tags[i];
				int valueType = message.getValueType(tags[i]);
				auto duration = std::chrono::high_resolution_clock::now().time_since_epoch();
				auto nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(duration);
				cout << nanoseconds.count() << " ";
				cout << subscription->getTopicName() << " " << tag << " " << valueType << " ";
				switch (valueType)
				{
				case TYPE_BOOLEAN:
					cout << message.extractBool(tag) << endl;
					break;
				case TYPE_FLOAT:
					cout << message.extractFloat(tag) << endl;
					break;
				case TYPE_DOUBLE:
					cout << message.extractDouble(tag) << endl;
					break;
				case TYPE_STRING:
					cout << message.extractString(tag) << endl;
					break;
				default:
					break;
				}

			}
		}

		/**
	* called when a publication subscribed to is available.
	* @param arg0 - the subsctiption who's publication is available.
	* */
		void connected(DipSubscription *arg0)
		{
		}

		/**
	* called when a publication subscribed to is unavailable.
	* @param arg0 - the subsctiption who's publication is unavailable.
	* @param arg1 - string providing more information about why the publication is unavailable.
	* */
		void disconnected(DipSubscription *arg0, char *arg1)
		{
			//printf("\nPublication source %s unavailable\n", arg0->getTopicName());
		}

		void handleException(DipSubscription *subscription, DipException &ex)
		{
			cout << "Error in subscription" << endl;
			//printf("Subs %s has error %s\n", subscription->getTopicName(), ex.what());
		}
	};

	//A handle on the DIP Data recipient.
	GeneralDataListener *handler;

public:
	/**
	* set up the subscriptions to dip/test/pub1 dip/test/pub2
	* */
	Client(const int argc, const char **argv)
	{
		dip = Dip::create("dip-client");
		dip->setDNSNode("dipnsgpn1,dipnsgpn2");
		handler = new GeneralDataListener(this);
		// Read from file
		// Check if there is a specified env variable for list.txt
		string filepath;
		if (std::getenv("LIST_PATH"))
		{
			const char* path = std::getenv("LIST_PATH");
			filepath = path;
		} else {
			// Default location fallback
			filepath = "/config/list.txt";
		}
		// count the number of lines to get the numberOfPubs
		ifstream infile(filepath.c_str());
		vector<string> subscriptions;
		string subscription;
		int numLines = 0;
		while (infile >> subscription) {
			subscriptions.push_back(subscription);
			++numLines;
		}
		sub = new DipSubscription*[numLines];
		for (size_t i = 0; i < subscriptions.size(); i++)
		{
			subscription = subscriptions[i];
			cout << "Subcription " << subscription << endl;
			sub[i] = dip->createDipSubscription(subscription.c_str(), handler);
		}
	}

	~Client()
	{
		dip->destroyDipSubscription(sub[0]);
		delete handler;
		delete dip;
		cout << "Client's done." << endl;
	}
};

/**
* Start the client
* */
int main(const int argc, const char **argv)
{
	Client *theClient = new Client(argc, argv);
	unsigned int lifeTime = 10;
	while(true) {
		sleep(lifeTime);
	}
	
	cout << "Client's lifetime has expired, leaving... " << endl;

	delete theClient;
	return (0);
}
